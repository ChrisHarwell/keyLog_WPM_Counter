#!/bin/bash

osascript -e 'tell application "System Events" to get name of first process whose frontmost is true'