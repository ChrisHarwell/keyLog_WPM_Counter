const fs = require('fs');
const { promisify } = require('util');
const readline = require('readline');
const { exec } = require('child_process');
const appendFile = promisify(fs.appendFile);

// Global variables
let start_time = null;
const typed_words = [];

// Function to calculate words per minute (WPM)
function calculateWPM(start, end, numWords) {
  const minutes = (end - start) / 60;
  const wpm = minutes > 0 ? Math.round(numWords / minutes) : 0;
  return wpm;
}

// Function to save results to files
async function saveResults(appName, wpm) {
  const datetime = new Date().toISOString();
  const textResult = `${appName}, WPM: ${wpm}, Date: ${datetime}\n`;
  const csvResult = `${appName},${wpm},${datetime}\n`;

  try {
    await appendFile('typing_results.txt', textResult);
    await appendFile('typing_results.csv', csvResult);
  } catch (error) {
    console.error('Error saving results:', error);
  }
}

// Function to get the name of the active application on macOS
function getAppName() {
  return new Promise((resolve, reject) => {
    exec('./getAppName.sh', (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else {
        resolve(stdout.trim());
      }
    });
  });
}

testing to see if this is actually working again.
// Create readline interface for capturing keyboard input
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false,
});

// Save results every 2 seconds
setInterval(async () => {
  if (start_time !== null) {
    const endTime = Math.floor(Date.now() / 1000);
    const wpm = calculateWPM(start_time, endTime, typed_words.length);
    const appName = await getAppName();
    await saveResults(appName, wpm);
  }
}, 2000);

// Start monitoring keyboard events
rl.on('line', async (line) => {
  if (line === ' ') {
    const currentTime = Math.floor(Date.now() / 1000);
    typed_words.push(currentTime);
    if (start_time === null) {
      start_time = currentTime;
    }
  } else if (line === '') {
    const endTime = Math.floor(Date.now() / 1000);
    const wpm = calculateWPM(start_time, endTime, typed_words.length);
    const appName = await getAppName();
    await saveResults(appName, wpm);
    typed_words.length = 0;
    start_time = null;
  }
});
