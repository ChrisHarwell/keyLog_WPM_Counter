#!/bin/bash

# Global variables
start_time=""
typed_words=()
max_execution_time=1200  # Maximum execution time in seconds (20 minutes)

# Function to calculate words per minute (WPM)
calculate_wpm() {
  local start=$1
  local end=$2
  local num_words=$3
  local minutes=$(( ($end - $start) / 10 ))
  local wpm=0
  if (( minutes > 0 )); then
    wpm=$(( num_words / minutes ))
  fi
  echo $wpm
}

# Function to save results to files
save_results() {
  local app_name=$1
  local wpm=$2
  local datetime=$(date +"%Y-%m-%d %H:%M:%S")
  echo "$app_name, WPM: $wpm, Date: $datetime" >> typing_results.txt
  echo "$app_name, $wpm, $datetime" >> typing_results.csv
}

# Function to get the name of the active application on macOS
get_app_name() {
  local app_name=$(osascript -e 'tell application "System Events" to get name of first process whose frontmost is true')
  echo "$app_name"
}

# Function to handle saving results and resetting variables
handle_results() {
  local app_name=$1
  local end_time=$(date +"%s")
  local wpm=$(calculate_wpm $start_time $end_time ${#typed_words[@]})
  save_results "$app_name" $wpm
  typed_words=()
  start_time=$end_time
}

# Start monitoring keyboard events
start_time=$(date +"%s")
app_name=$(get_app_name)

while true; do
  read -rsn1 key
  if [[ $key == " " ]]; then
    current_time=$(date +"%s")
    typed_words+=($current_time)
  elif [[ $key == $'\x0a' ]]; then
    end_time=$(date +"%s")
    if (( end_time - start_time >= 60 )); then
      handle_results "$app_name"
    fi
  fi

  # Check if maximum execution time has been reached
  current_time=$(date +"%s")
  elapsed_time=$((current_time - start_time))
  if ((elapsed_time >= max_execution_time)); then
    handle_results "$app_name"
    break
  fi
done
