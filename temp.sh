#!/bin/bash

# Global variables
start_time=""
typed_words=()

# Function to calculate words per minute (WPM)
calculate_wpm() {
  local start=$1
  local end=$2
  local num_words=$3
  local minutes=$(( ($end - $start) / 60 ))
  local wpm=0
  if (( minutes > 0 )); then
    wpm=$(( num_words / minutes ))
  fi
  echo $wpm
}

# Function to save results to files
save_results() {
  local app_name=$1
  local wpm=$2
  local datetime=$(date +"%Y-%m-%d %H:%M:%S")
  echo "$app_name, WPM: $wpm, Date: $datetime" >> typing_results.txt
  echo "$app_name, $wpm, $datetime" >> typing_results.csv
}

# Function to get the name of the active application on macOS
get_app_name() {
  local app_name=$(osascript -e 'tell application "System Events" to get name of first process whose frontmost is true')
  echo $app_name
}

# Trap SIGINT to handle script termination
trap "save_results 'Your Application' 0; exit" SIGINT

# Start monitoring keyboard events
while true; do
  read -rsn1 key
  if [[ $key == " " ]]; then
    current_time=$(date +"%s")
    typed_words+=($current_time)
    if [[ -z $start_time ]]; then
      start_time=$current_time
    fi
  elif [[ $key == $'\x0a' ]]; then
    end_time=$(date +"%s")
    wpm=$(calculate_wpm $start_time $end_time ${#typed_words[@]})
    app_name=$(get_app_name)
    save_results "$app_name" $wpm
    typed_words=()
    start_time=""
  fi
done

